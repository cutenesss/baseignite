/* eslint-disable no-console */
/* eslint-disable consistent-return */
/* eslint-disable no-return-await */
/**
 * helper.js - for storing reusable logic.
 */
import axios from "axios"
import { Dimensions } from 'react-native'

export async function PostData(url, json, isAuth = true) {
  const myRequest = {
    method: "post",
    url,
    headers: {
      "Content-Type": "application/json"
    },
    data: JSON.stringify(json)
  }
  return await axios(myRequest)
    .then(response => response)
    .then(response => {
      return response
    })
}

const { width, height } = Dimensions.get('window')
export const WIDTH = (w) => width * (w / 360)
export const HEIGHT = (h) => height * (h / 640)
export const getFont = (f) => f - 1
export const getLineHeight = (f) => f
export const getHeight = () => height
export const getWidth = () => width
