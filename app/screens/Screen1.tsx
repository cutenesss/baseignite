import React, { useState } from 'react'
import { StyleSheet, TouchableWithoutFeedback, Keyboard } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'
import { ApplicationProvider, Layout, Text, Input, Button, Spinner } from '@ui-kitten/components'
import { getWidth, HEIGHT, WIDTH } from '../config/helper'
import { login } from '../redux/actions/userAction'
import * as eva from '@eva-design/eva'

const Screen1 = () => {
  const account = useSelector((state: any) => state.userReducer.account)
  const loading = useSelector((state: any) => state.userReducer.loading)
  const [user, setUser] = useState('')
  const [pass, setPass] = useState('')
  const dispatch = useDispatch()

  const onLogin = () => {
    dispatch(login({
      username: user,
      password: pass
    }))
  }

  return (
    <ApplicationProvider {...eva} theme={eva.dark}>
    <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false} testID="Hide">
        <Layout style={styles.container}>
          <Input
            testID="InputUser"
            style={styles.input}
            textStyle={styles.text}
            onChangeText={(text) => setUser(text)}
            label={() => <Text>Username</Text>}
          />
          <Input
            testID="InputPass"
            style={[styles.input, { marginTop: HEIGHT(10) }]}
            onChangeText={(text) => setPass(text)}
            textStyle={styles.text}
            secureTextEntry={true}
            autoCapitalize='none'
            label={() => <Text>Password</Text>}
          />
          <Button style={styles.btn} onPress={onLogin} testID="LoginBtn">
            {() => <Text>Log in</Text>}
          </Button>
          {
            loading && (
              <Layout style={{
                elevation: 3,
                width: getWidth(),
                flex: 1,
                zIndex: 10,
                position: 'absolute',
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: 'transcend'
              }}>
                <Spinner
                  size='large'
                  animating={loading}
                />
              </Layout>
            )
          }
        </Layout>
      </TouchableWithoutFeedback>
      </ApplicationProvider>
  )
}

export default Screen1

const styles = StyleSheet.create({
  btn: {
    borderRadius: WIDTH(20),
    marginTop: HEIGHT(10),
    width: getWidth() * 3 / 10
  },
  container: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center'
  },
  input: {
    backgroundColor: 'white',
    width: getWidth() * 9 / 10
  },
  spinner: {
    color: 'red',
  },
  text: {
    color: 'black'
  }
})
