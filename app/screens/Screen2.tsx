import React from 'react'
import { View, Text } from 'react-native'
import { useSelector } from 'react-redux'

const Screen2 = () => {
  const account = useSelector((state: any) => state.userReducer.account)
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>{account?.user?.hoTen ?? 'Trong'}</Text>
      </View>
  )
}

export default Screen2
