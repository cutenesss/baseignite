import { Alert } from 'react-native'
import { takeLatest, call, put } from 'redux-saga/effects'
import { PostData } from '../../config/helper'
import NavigationService from '../../navigation/navigation-utilities'
import * as Action from '../actions/actionTypes'

const loginAPI = body =>
  PostData(`https://ftudev.aisenote.com/auth/login`, body, false).then(res => res)

function * fetchLoginAPI(action) {
  try {
    const response = yield call(loginAPI, action.content)
    yield put({ type: Action.GET_ACCOUNT_SUCCESS, payload: response.data })
    NavigationService.navigate('Screen2')
  } catch (err) {
    Alert.alert('Notification', err?.response?.data?.error?.message ?? 'Log in failed')
    yield put({ type: Action.GET_ACCOUNT_FAILED })
  }
}

export function * fetchLogin() {
  yield takeLatest(Action.GET_ACCOUNT, fetchLoginAPI)
}
