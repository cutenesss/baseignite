import { all, fork } from 'redux-saga/effects'
import { fetchLogin } from './Login'

export default function * rootSaga() {
  yield all([
    fork(fetchLogin),
  ])
}
