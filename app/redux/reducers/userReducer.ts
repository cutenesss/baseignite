import * as Action from '../actions/actionTypes'

const initialState = {
  account: {},
  loading: false
}

const userReducer = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case Action.GET_ACCOUNT:
      return {
        ...state,
        loading: true
      }
    case Action.GET_ACCOUNT_SUCCESS:
      return {
        ...state,
        account: action.payload,
        loading: false
      }
    case Action.GET_ACCOUNT_FAILED:
      return {
        ...state,
        loading: false
      }
  }
  return state
}

export default userReducer
