// For more info on how to write Detox tests, see the official docs:
// https://github.com/wix/Detox/blob/master/docs/README.md

const { reloadApp } = require("./reload")

describe("Screen1", () => {
  beforeEach(async () => {
    await reloadApp()
  })

  it("screen1 should be visible", async () => {
    await expect(element(by.id("InputUser"))).toBeVisible()
    await expect(element(by.id("InputPass"))).toBeVisible()
    await expect(element(by.id("LoginBtn"))).toBeVisible()
  })

  it("should go to next screen after login", async () => {
    await element(by.id('InputUser')).typeText(`1911110003`);
    await element(by.id('InputPass')).typeText(`ehomeftu`);
    await element(by.id('Hide')).tap({x:5, y:10});
    await element(by.id("LoginBtn")).tap()
    await expect(element(by.text("Mạc Thị Thu An"))).toBeVisible()
  })
})
